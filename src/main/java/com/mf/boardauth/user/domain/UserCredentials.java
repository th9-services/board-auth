package com.mf.boardauth.user.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserCredentials {
    private String username;
    private String password;
    private UserType userType;
}
