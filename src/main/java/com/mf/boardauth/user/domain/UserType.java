package com.mf.boardauth.user.domain;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

public enum UserType implements GrantedAuthority {
    ORDINARY_PLAYER("ORDINARY_PLAYER"),
    GAME_MASTER("GAME_MASTER"),
    SUPER_USER("SUPER_USER");

    @Getter
    private String authority;

    UserType(final String authority) {
        this.authority = authority;
    }
}
