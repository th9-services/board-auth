package com.mf.boardauth.user.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class UserInfo {
    private String username;
    private String nickname;
    private LocalDateTime joinTime;
}
