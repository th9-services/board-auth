package com.mf.boardauth.user.domain;

import lombok.AllArgsConstructor;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.mf.boardauth.user.persistence.UserEntity;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

@AllArgsConstructor
@ToString
public class BoardUserDetails implements UserDetails {
    private UserEntity userEntity;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(userEntity.getUserType());
    }

    @Override
    public String getPassword() {
        return userEntity.getPassword();
    }

    @Override
    public String getUsername() {
        return userEntity.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getRoomId() {
        return userEntity.getRoomId();
    }

    public String getNickname() {
        return userEntity.getNickname();
    }

    public UserType getUserType() {
        return userEntity.getUserType();
    }

    public LocalDateTime getJoinTime() {
        return userEntity.getJoinTime();
    }

    public UserInfo asUserInfo() {
        return new UserInfo(getUsername(), getNickname(), getJoinTime());
    }

    public UserCredentials asUserCredentials() {
        return new UserCredentials(getUsername(), getPassword(), getUserType());
    }

    public boolean isInRoom(final String roomId) {
        return getRoomId().equals(roomId);
    }

}
