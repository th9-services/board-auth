package com.mf.boardauth.user.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.mf.boardauth.user.persistence.UserEntity;
import com.mf.boardauth.user.persistence.UserRepository;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BoardUserService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final UserEntity userEntity = this.userRepository.findByUsername(username);

        if (userEntity == null) {
            throw new UsernameNotFoundException(username);
        }
        return new BoardUserDetails(userEntity);
    }

    public void deleteByRoomId(final String roomId) {
        this.userRepository.deleteByRoomId(roomId);
    }

    public void deletePlayer(final BoardUserDetails player) {
        userRepository.deleteByUsername(player.getUsername());
    }

    public BoardUserDetails generateGameMaster(final String roomId, final String nickname) {
        return this.generateUser(roomId, nickname, UserType.GAME_MASTER);
    }

    public BoardUserDetails generatePlayer(final String roomId, final String nickname) {
        return this.generateUser(roomId, nickname, UserType.ORDINARY_PLAYER);
    }

    private BoardUserDetails generateUser(final String roomId, final String nickname, final UserType userType) {
        final UserEntity userEntity = new UserEntity(
                UUID.randomUUID().toString(),
                "{noop}" + UUID.randomUUID(),
                roomId,
                nickname,
                userType,
                LocalDateTime.now()
        );
        this.userRepository.save(userEntity);

        return new BoardUserDetails(userEntity);
    }
}
