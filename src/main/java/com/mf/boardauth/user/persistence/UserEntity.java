package com.mf.boardauth.user.persistence;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import com.mf.boardauth.user.domain.UserType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserEntity {
    @Id
    @Column(nullable = false, unique = true)
    private String username;
    private String password;
    private String roomId;
    private String nickname;
    private UserType userType;
    private LocalDateTime joinTime;
}

